<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

    function ubah_huruf($string){
        $abjad = "abcdefghijklmnopqrstuvwxyz";
        $output = "";
        for ($i = 0; $i < strlen($string); $i++){
            $position = strpos($abjad, $string[$i]);
            $output .= substr($abjad, $position +1, 1);
        }
        return $output ."<br>";
    }
    echo "wow : ", ubah_huruf('wow'); // xpx
    echo "developer : ", ubah_huruf('developer'); // efwfmpqfs
    echo "laravel : ", ubah_huruf('laravel'); // mbsbwfm
    echo "keren : ", ubah_huruf('keren'); // lfsfo
    echo "semangat : ", ubah_huruf('semangat'); // tfnbohbu


    ?>
</body>
</html>